#!/bin/sh

# install caddy
apt install caddy -y

#download
##diffuse
if [ ! -f "/opt/build/index.html" ];then
    curl -L https://github.com/icidasset/diffuse/releases/download/3.2.0/diffuse-web.tar.gz -o diffuse-web.tar.gz
    tar -zxvf diffuse-web.tar.gz
    rm -f diffuse-web.tar.gz
fi

##verysimple
if [ ! -f "/opt/verysimple" ];then
    curl -L https://github.com/e1732a364fed/v2ray_simple/releases/download/v1.2.4-beta.3/verysimple_linux_amd64.tar.xz -o verysimple.tar.xz
    tar -xvf verysimple.tar.xz
    rm -f verysimple.tar.xz
    chmod +x verysimple
fi

# configs
ParameterSSENCYPT=chacha20-ietf-poly1305

if [ $uuid ];then
    cat > /opt/server.toml <<EOF
[[listen]]
protocol = "vless"
uuid = "$uuid"
host = "0.0.0.0"
port = 23333
insecure = true
fallback = ":80"
# cert = "cert.pem"
# key = "cert.key"
advancedLayer = "ws"
path = "$path"
fullcone = true
# early = true

[[dial]]
protocol = "direct"
fullcone = true
EOF
fi

if [ ! -f "/opt/caddyfile" ];then
    cat > /opt/caddyfile <<EOF
:80
root * /opt/build
file_server browse

header {
    X-Robots-Tag none
    X-Content-Type-Options nosniff
    X-Frame-Options DENY
    Referrer-Policy no-referrer-when-downgrade
}

@websocket_verysimple {
        path $path
        header Connection *Upgrade*
        header Upgrade websocket
    }
reverse_proxy @websocket_verysimple unix//etc/caddy/vless
EOF
fi


# start

/verysimple -c /opt/server.toml &
caddy run --config /opt/caddyfile --adapter caddyfile
